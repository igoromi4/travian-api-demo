"""
Copyright © 2017 Ihor Omelchenko <counter3d@gmail.com>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
"""

import time
import os.path
import configparser
import threading
import tkinter

try:
    import pushbullet
except ImportError:
    pushbullet = None
    print("No module pushbullet")

import travianapi
from travianapi.account import Account

__VERSION__ = '0.1.0'

CONFIG_FILEPATH = 'config.cfg'


class Config(configparser.ConfigParser):
    def __init__(self, config_path: str):
        configparser.ConfigParser.__init__(self)

        self.config_path = config_path

        self._check_template()

    def is_file_exist(self):
        return os.path.exists(self.config_path)

    def _check_template(self):
        if self.is_file_exist():
            self.load()

        if not 'APP' in self:
            self['APP'] = {}

        if not 'url' in self['APP']: 
            self['APP']['url'] = ''
        if not 'nickname' in self['APP']: 
            self['APP']['nickname'] = ''
        if not 'password' in self['APP']: 
            self['APP']['password'] = ''

        if not 'period' in self['APP']: 
            self['APP']['period'] = '10'

        if not 'PUSHBULLET' in self:
            self['PUSHBULLET'] = {}

        if not 'api_key' in self['PUSHBULLET']: 
            self['PUSHBULLET']['api_key'] = ''

        self.save()

    def load(self):
        self.read(self.config_path)

    def save(self):
        with open(self.config_path, 'w') as file:
            self.write(file)


class Application:
    def __init__(self):
        self.account = None

        self.config = Config(CONFIG_FILEPATH)

        # init gui
        self.init_gui()

        self.load_config()

        # init threads
        self.event = threading.Event()

        self.thread = threading.Thread(target=self.thread_function_overwatch)
        self.thread.daemon = True
        self.thread.start()

    def init_gui(self):
        self.win = tkinter.Tk()

        self.win.title('Travian Bot Demo v{}'.format(__VERSION__))

        self.label_url = tkinter.Label(self.win, text="Url:")
        self.label_url.pack()

        self.entry_url = tkinter.Entry(self.win)
        self.entry_url.pack()

        self.label_nickname = tkinter.Label(self.win, text="Nickname:")
        self.label_nickname.pack()

        self.entry_nickname = tkinter.Entry(self.win)
        self.entry_nickname.pack()

        self.label_password = tkinter.Label(self.win, text="Password:")
        self.label_password.pack()

        self.entry_password = tkinter.Entry(self.win)
        self.entry_password.pack()

        self.label_pb_api_key = tkinter.Label(self.win, text="Pushbullet API key:")
        self.label_pb_api_key.pack()

        self.entry_pb_api_key = tkinter.Entry(self.win)
        self.entry_pb_api_key.pack()

        self.button_login = tkinter.Button(self.win, 
            text='Login', 
            command=self.login,
            state=tkinter.NORMAL)  # NORMAL, DISABLED
        self.button_login.pack()

    def mainloop(self):
        self.win.mainloop()

    def send_message(self, message: str=''):
        if not type(message) is str:
            raise TypeError()

        print('message:', message)

        if 'PUSHBULLET' in self.config and 'api_key' in self.config['PUSHBULLET']:

            pb_api_key = self.config['PUSHBULLET']['api_key']

            if pb_api_key:
                push = pushbullet.Pushbullet(pb_api_key)

                push.push_note("Travian Overwatch", message)

    def load_config(self):
        if 'APP' in self.config:
            url = self.config['APP']['url']
            nickname = self.config['APP']['nickname']
            password = self.config['APP']['password']

            self.entry_url.insert(0, url)
            self.entry_nickname.insert(0, nickname)
            self.entry_password.insert(0, password)

            self.period = int(self.config['APP']['period'])  # sec

    def save_config(self):
        self.config['APP']['url'] = self.entry_url.get()
        self.config['APP']['nickname'] = self.entry_nickname.get()
        self.config['APP']['password'] = self.entry_password.get()

        self.config.save()

    def login(self):
        url = self.entry_url.get()
        username = self.entry_nickname.get()
        password = self.entry_password.get()

        self.save_config()

        self.account = Account(url, username, password)

        if not self.account.login.login():
            print('Can\'t login')
            return

        print('Logined:', username)

        self.event.set()

    def thread_function_overwatch(self):
        self.event.wait()

        while True:
            for v in self.account.villages:
                movements = v.troops.get_movements()
                self.send_message(str(movements))
            time.sleep(self.period)

app = Application()

app.mainloop()

